// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6.1 (swiftlang-5.6.0.323.66 clang-1316.0.20.12)
// swift-module-flags: -target arm64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Osize -module-name madme_ios_sdk
import AVFoundation
import AdSupport
import BackgroundTasks
import CallKit
import SystemConfiguration.CaptiveNetwork
import CoreData
import CoreGraphics
import CoreLocation
import CoreTelephony
import CryptoKit
import Darwin
import Foundation
import Fuzi
import MessageUI
import MetricKit
import MobileCoreServices
import QuartzCore
import Reachability
import Security
import Swift
import SwiftUI
import SwiftyRSA
import SystemConfiguration
import UIKit
import UserNotifications
import WebKit
import _AVKit_SwiftUI
import _Concurrency
import _MapKit_SwiftUI
@_exported import madme_ios_sdk
import madme_ios_sdk_shared
extension CallKit.CXCall {
  @objc override dynamic open var debugDescription: Swift.String {
    @objc get
  }
}
extension UIKit.UINavigationController {
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var childForStatusBarStyle: UIKit.UIViewController? {
    @_Concurrency.MainActor(unsafe) @objc get
  }
}
@objc public enum MMUserAccountStatus : Swift.Int {
  case unknown
  case activated
  case suspended
  case terminated
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol MMUserAccountStatusChangeContextProtocol {
  @objc var correlationIdentifier: Swift.String { get }
  @objc var source: Swift.String { get }
  @objc var loggingParameters: [Swift.String : Swift.AnyObject] { get }
}
@_hasMissingDesignatedInitializers @objc public class MMGenericUserAccountStatusChangeContext : madme_ios_sdk.MMPrivateInitObject, madme_ios_sdk.MMUserAccountStatusChangeContextProtocol {
  @objc public init(correlationIdentifier _correlationIdentifier: Swift.String, source _source: Swift.String)
  @objc final public let correlationIdentifier: Swift.String
  @objc final public let source: Swift.String
  @objc public var loggingParameters: [Swift.String : Swift.AnyObject] {
    @objc get
  }
  @objc deinit
}
@objc public protocol MMUserAccountStatusDelegateProtocol {
  @objc optional func willPerformUserAccountStatusChange(to userAccountStatus: madme_ios_sdk.MMUserAccountStatus, closure: @escaping (_ abort: Swift.Bool) -> Swift.Void)
}
extension madme_ios_sdk.MMSdk {
  @objc final public func getUserAccountStatus(closure: @escaping (_ userAccountStatus: madme_ios_sdk.MMUserAccountStatus) -> Swift.Void)
  @objc final public func changeUserAccountStatus(to userAccountStatus: madme_ios_sdk.MMUserAccountStatus, changeContext: madme_ios_sdk.MMUserAccountStatusChangeContextProtocol, closure: @escaping (_ userAccountStatus: madme_ios_sdk.MMUserAccountStatus) -> Swift.Void)
}
@objc public enum MMHomeScreenOptionType : Swift.Int {
  case myBenefits
  case showOffer
  case activeCampaigns
  case offersHistory
  case editProfile
  case termsAndConditions
  case buildConfiguration
  @available(iOS 13.0.0, *)
  case metrics
  @available(iOS 13.0.0, *)
  case urlCalls
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers @objc final public class MMHomeScreenOption : madme_ios_sdk.MMPrivateInitObject {
  @objc public init(label: Swift.String, oDetailLabel: Swift.String? = nil, optionType: madme_ios_sdk.MMHomeScreenOptionType, oImage: UIKit.UIImage? = nil)
  @objc deinit
}
@objc public enum MMAggregateQualifier : Swift.Int {
  case any
  case all
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension madme_ios_sdk.MMAggregateQualifier : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
@objc public protocol MMClientIdentityProviderProtocol {
  @objc var oAuth2Id: Swift.String { get }
  @objc var oAuth2Secret: Swift.String { get }
  @objc optional var oClientIds: [Swift.String : Swift.String]? { get }
}
@objc public protocol MMSdkDelegateProtocol : madme_ios_sdk.MMCampaignContextProviderProtocol, madme_ios_sdk.MMClientIdentityProviderProtocol, madme_ios_sdk.MMUserAccountStatusDelegateProtocol {
}
@_hasMissingDesignatedInitializers @objc final public class MMSdk : madme_ios_sdk.MMPrivateInitObject {
  @objc public static let sharedInstance: madme_ios_sdk.MMSdk
  @objc final public let version: Swift.String
  @objc weak final public var delegate: madme_ios_sdk.MMSdkDelegateProtocol! {
    @objc get
    @objc set
  }
  @objc final public func presentAd(tags: Swift.Set<Swift.String> = Set<String>(), tagsAggregateQualifier: madme_ios_sdk.MMAggregateQualifier = .any, adPresentationMode: madme_ios_sdk.MMAdPresentationMode = .direct, oParentViewController: UIKit.UIViewController? = nil, oResultClosure: ((_ adPresentationResult: madme_ios_sdk.MMAdPresentationResultProtocol) -> Swift.Void)? = nil)
  @objc final public func process(notificationPayload: [Swift.AnyHashable : Any], oActionIdentifier: Swift.String? = nil, oResultClosure: ((_ notificationProcessingResult: madme_ios_sdk.MMNotificationProcessingResultProtocol) -> Swift.Void)? = nil)
  @objc final public func configureRemoteNotifications(with remoteNotificationsRegistrationToken: Swift.String)
  @objc final public func process(url: Foundation.URL, oResultClosure: ((_ urlProcessingResult: madme_ios_sdk.MMUrlProcessingResultProtocol) -> Swift.Void)? = nil) -> Swift.Bool
  @objc final public func handleEventsForBackgroundURLSession(identifier: Swift.String, resultClosure: @escaping (_ urlSessionEventsProcessingResult: madme_ios_sdk.MMUrlSessionEventsProcessingResultProtocol) -> Swift.Void)
  @objc final public func application(_ application: UIKit.UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIKit.UIBackgroundFetchResult) -> Swift.Void)
  @objc final public func homeScreenViewController(options: [madme_ios_sdk.MMHomeScreenOption]) -> UIKit.UIViewController
  @objc(configureButton:withUIAttributesId:) final public func configure(button: UIKit.UIButton, withUIAttributesId id: Swift.String)
  @objc(configureTextField:withUIAttributesId:) final public func configure(textField: UIKit.UITextField, withUIAttributesId id: Swift.String)
  @objc deinit
}
@objc extension madme_ios_sdk.MMSdk {
  @objc final public var managesStatusBarStyle: Swift.Bool {
    @objc get
  }
  @objc final public var configurationStatusBarStyle: UIKit.UIStatusBarStyle {
    @objc get
  }
}
extension Foundation.NSNotification.Name {
  public static let Campaigns: Foundation.Notification.Name
}
@objc extension Foundation.NSNotification {
  @objc public static let Campaigns: Foundation.Notification.Name
}
@objc extension madme_ios_sdk.MMSdk {
  @objc public static let CampaignsKey: Swift.String
}
@_hasMissingDesignatedInitializers @objc final public class MMCampaign : ObjectiveC.NSObject {
  @objc final public let campaignId: Swift.String
  @objc final public let oDescription: Swift.String?
  @objc final public let oThumbnail: UIKit.UIImage?
  @objc final public let validUntil: Foundation.Date
  @objc final public let isFavorite: Swift.Bool
  @objc final public let tags: Swift.Set<Swift.String>
  @objc deinit
}
@objc extension madme_ios_sdk.MMCampaign {
  @objc override final public var description: Swift.String {
    @objc get
  }
}
@available(iOS 13.0, *)
extension madme_ios_sdk.MMSdk {
  @available(iOS 13.0, *)
  @objc public static func configureBackgroundRefresh()
}
@available(iOS 13.0, *)
extension madme_ios_sdk.MMSdk {
  @available(iOS 13.0, *)
  @objc public static func configureBackgroundProcessing()
}
@objc public protocol MMCampaignContextProviderProtocol {
  @objc optional var campaignContext: madme_ios_sdk.MMCampaignContextProtocol { get }
}
@objc public protocol MMCampaignContextProtocol {
  @objc var typeRawValue: Swift.String { get }
}
@objc @_inheritsConvenienceInitializers final public class MMViewedNoSurveyNoRatingCampaignContext : ObjectiveC.NSObject, madme_ios_sdk.MMCampaignContextProtocol {
  @objc final public var typeRawValue: Swift.String {
    @objc get
  }
  @objc override dynamic public init()
  @objc deinit
}
extension madme_ios_sdk.MMSdk {
  @objc public static func configureNotificationCategories()
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class MMError : Foundation.NSError {
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc final public class MMAdPresentationError : madme_ios_sdk.MMError {
  @objc public static let noAdAvailable: madme_ios_sdk.MMError
  @objc public static let unsupportedAd: madme_ios_sdk.MMError
  @objc public static let adNotPresentable: madme_ios_sdk.MMError
  @objc public static let accountNotActivated: madme_ios_sdk.MMError
  @objc public static let appAsleep: madme_ios_sdk.MMError
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc final public class MMNotificationProcessingError : madme_ios_sdk.MMError {
  @objc public static let unsupportedNotification: madme_ios_sdk.MMError
  @objc public static let failedToRetrieveMedia: madme_ios_sdk.MMError
  @objc public static let failedToScheduleLocalNotification: madme_ios_sdk.MMError
  @objc public static let failedToProcessAction: madme_ios_sdk.MMError
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc final public class MMUrlProcessingError : madme_ios_sdk.MMError {
  @objc public static let unsupportedUrl: madme_ios_sdk.MMError
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc final public class MMUrlSessionEventsProcessingError : madme_ios_sdk.MMError {
  @objc public static let unsupportedUrlSessionIdentifier: madme_ios_sdk.MMError
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class MMPrivateInitObject : ObjectiveC.NSObject {
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
  @objc deinit
}
extension Swift.Array where Element == Swift.UInt8 {
  public func toHexString() -> Swift.String
}
extension Swift.Array where Element == Swift.UInt8 {
  public func md5() -> [Element]
  public func sha1() -> [Element]
  public func sha256() -> [Element]
}
public enum Bit : Swift.Int {
  case zero
  case one
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public struct Digest {
  public static func md5(_ bytes: [Swift.UInt8]) -> [Swift.UInt8]
  public static func sha1(_ bytes: [Swift.UInt8]) -> [Swift.UInt8]
  public static func sha256(_ bytes: [Swift.UInt8]) -> [Swift.UInt8]
  public static func sha2(_ bytes: [Swift.UInt8], variant: madme_ios_sdk.SHA2.Variant) -> [Swift.UInt8]
}
final public class MD5 {
  public init()
  final public func calculate(for bytes: [Swift.UInt8]) -> [Swift.UInt8]
  @objc deinit
}
extension madme_ios_sdk.MD5 : madme_ios_sdk.Updatable {
  final public func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool = false) throws -> [Swift.UInt8]
}
extension Swift.String {
  public var bytes: [Swift.UInt8] {
    get
  }
  public func md5() -> Swift.String
  public func sha1() -> Swift.String
  public func sha256() -> Swift.String
}
final public class SHA1 {
  public init()
  final public func calculate(for bytes: [Swift.UInt8]) -> [Swift.UInt8]
  @objc deinit
}
extension madme_ios_sdk.SHA1 : madme_ios_sdk.Updatable {
  @discardableResult
  final public func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool = false) throws -> [Swift.UInt8]
}
public protocol Updatable {
  mutating func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool) throws -> [Swift.UInt8]
  mutating func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool, output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
  mutating func finish(withBytes bytes: Swift.ArraySlice<Swift.UInt8>) throws -> [Swift.UInt8]
  mutating func finish(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
}
extension madme_ios_sdk.Updatable {
  public mutating func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool = false, output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
  @discardableResult
  public mutating func finish(withBytes bytes: Swift.ArraySlice<Swift.UInt8>) throws -> [Swift.UInt8]
  @discardableResult
  public mutating func finish() throws -> [Swift.UInt8]
  public mutating func finish(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
  public mutating func finish(output: ([Swift.UInt8]) -> Swift.Void) throws
}
extension madme_ios_sdk.Updatable {
  @discardableResult
  public mutating func update(withBytes bytes: [Swift.UInt8], isLast: Swift.Bool = false) throws -> [Swift.UInt8]
  public mutating func update(withBytes bytes: [Swift.UInt8], isLast: Swift.Bool = false, output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
  @discardableResult
  public mutating func finish(withBytes bytes: [Swift.UInt8]) throws -> [Swift.UInt8]
  public mutating func finish(withBytes bytes: [Swift.UInt8], output: (_ bytes: [Swift.UInt8]) -> Swift.Void) throws
}
extension Foundation.Data {
  public func md5() -> Foundation.Data
  public func sha1() -> Foundation.Data
  public func sha256() -> Foundation.Data
}
extension Foundation.Data {
  public var bytes: [Swift.UInt8] {
    get
  }
}
final public class SHA2 {
  public enum Variant : Swift.RawRepresentable {
    case sha224, sha256, sha384, sha512
    public var digestLength: Swift.Int {
      get
    }
    public var blockSize: Swift.Int {
      get
    }
    public typealias RawValue = Swift.Int
    public var rawValue: madme_ios_sdk.SHA2.Variant.RawValue {
      get
    }
    public init?(rawValue: madme_ios_sdk.SHA2.Variant.RawValue)
  }
  public init(variant: madme_ios_sdk.SHA2.Variant)
  final public func calculate(for bytes: [Swift.UInt8]) -> [Swift.UInt8]
  @objc deinit
}
extension madme_ios_sdk.SHA2 : madme_ios_sdk.Updatable {
  final public func update(withBytes bytes: Swift.ArraySlice<Swift.UInt8>, isLast: Swift.Bool = false) throws -> [Swift.UInt8]
}
@objc public enum MMAdPresentationResultState : Swift.Int {
  case adNotPresented
  case adViewed
  case adTapped
  case adViewedAndTapped
  case adDismissed
  case adNotified
  case adNotifyFailed
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public typealias MMIdentifier = Swift.String
@objc public protocol MMAdPresentationResultProtocol {
  @objc var identifier: madme_ios_sdk.MMIdentifier { get }
  @objc var state: madme_ios_sdk.MMAdPresentationResultState { get }
  @objc var oValue: Swift.AnyObject? { get }
}
@objc public enum MMAdPresentationMode : Swift.Int {
  case direct
  case notification
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum MMUrlSessionEventsProcessingResultState : Swift.Int {
  case eventsNotProcessed
  case eventsProcessedSucessfully
  case eventsProcessedUnsuccessfully
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol MMUrlSessionEventsProcessingResultProtocol {
  @objc var state: madme_ios_sdk.MMUrlSessionEventsProcessingResultState { get }
  @objc var oValue: Swift.AnyObject? { get }
}
@objc public enum MMUrlProcessingResultState : Swift.Int {
  case urlNotProcessed
  case urlProcessedSucessfully
  case urlProcessedUnsuccessfully
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol MMUrlProcessingResultProtocol {
  @objc var state: madme_ios_sdk.MMUrlProcessingResultState { get }
  @objc var oValue: Swift.AnyObject? { get }
}
@objc public enum MMNotificationProcessingResultState : Swift.Int {
  case notificationNotProcessed
  case notificationProcessedSucessfully
  case notificationProcessedUnsuccessfully
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol MMNotificationProcessingResultProtocol {
  @objc var identifier: madme_ios_sdk.MMIdentifier { get }
  @objc var state: madme_ios_sdk.MMNotificationProcessingResultState { get }
  @objc var oValue: Swift.AnyObject? { get }
}
extension madme_ios_sdk.MMUserAccountStatus : Swift.Equatable {}
extension madme_ios_sdk.MMUserAccountStatus : Swift.Hashable {}
extension madme_ios_sdk.MMUserAccountStatus : Swift.RawRepresentable {}
extension madme_ios_sdk.MMHomeScreenOptionType : Swift.Equatable {}
extension madme_ios_sdk.MMHomeScreenOptionType : Swift.Hashable {}
extension madme_ios_sdk.MMHomeScreenOptionType : Swift.RawRepresentable {}
extension madme_ios_sdk.MMAggregateQualifier : Swift.Equatable {}
extension madme_ios_sdk.MMAggregateQualifier : Swift.Hashable {}
extension madme_ios_sdk.MMAggregateQualifier : Swift.RawRepresentable {}
extension madme_ios_sdk.Bit : Swift.Equatable {}
extension madme_ios_sdk.Bit : Swift.Hashable {}
extension madme_ios_sdk.Bit : Swift.RawRepresentable {}
extension madme_ios_sdk.SHA2.Variant : Swift.Equatable {}
extension madme_ios_sdk.SHA2.Variant : Swift.Hashable {}
extension madme_ios_sdk.MMAdPresentationResultState : Swift.Equatable {}
extension madme_ios_sdk.MMAdPresentationResultState : Swift.Hashable {}
extension madme_ios_sdk.MMAdPresentationResultState : Swift.RawRepresentable {}
extension madme_ios_sdk.MMAdPresentationMode : Swift.Equatable {}
extension madme_ios_sdk.MMAdPresentationMode : Swift.Hashable {}
extension madme_ios_sdk.MMAdPresentationMode : Swift.RawRepresentable {}
extension madme_ios_sdk.MMUrlSessionEventsProcessingResultState : Swift.Equatable {}
extension madme_ios_sdk.MMUrlSessionEventsProcessingResultState : Swift.Hashable {}
extension madme_ios_sdk.MMUrlSessionEventsProcessingResultState : Swift.RawRepresentable {}
extension madme_ios_sdk.MMUrlProcessingResultState : Swift.Equatable {}
extension madme_ios_sdk.MMUrlProcessingResultState : Swift.Hashable {}
extension madme_ios_sdk.MMUrlProcessingResultState : Swift.RawRepresentable {}
extension madme_ios_sdk.MMNotificationProcessingResultState : Swift.Equatable {}
extension madme_ios_sdk.MMNotificationProcessingResultState : Swift.Hashable {}
extension madme_ios_sdk.MMNotificationProcessingResultState : Swift.RawRepresentable {}
