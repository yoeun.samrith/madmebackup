//
//  madme_ios_sdk.h
//  madme_ios_sdk
//
//  Created by Razvan Popescu on 21/09/2015.
//  Copyright © 2015 MadMe. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for madme_ios_sdk.
FOUNDATION_EXPORT double madme_ios_sdkVersionNumber;

//! Project version string for madme_ios_sdk.
FOUNDATION_EXPORT const unsigned char madme_ios_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <madme_ios_sdk/PublicHeader.h>
