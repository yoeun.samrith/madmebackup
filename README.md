# README #

![Image](mAdmeLogo.png)

### Confidentiality ###

This Document is confidential to mAdme and its affiliates ("mAdme") and is provided by mAdme directly to the recipient on the strict understanding that no part of this document may be modified, translated, decompiled, disassembled, reproduced, used or transmitted by the recipient in any form without the prior written consent of mAdme. 

© 2022 mAdme. All rights reserved.

### mAdme iOS SDK Integration Steps ###

1.  Install Xcode 13.4.1 (iOS 11.0+, iOS 15.5 SDK, Swift 5.6.1 compiler).

2.  Install Cocoapods 1.11.3.

3.  Make sure you have Bitbucket and GitHub access (https/ssh).

4.  Define a Podfile similar to the following (replace project and target names):

		platform :ios, '11.0'

		use_frameworks!

		source 'https://cdn.cocoapods.org/'
		source 'https://bitbucket.org/madme/madme_ios_framework_podspecs.git'

		project 'mAdme Sample Objc App.xcodeproj'

		def pods
		    # Core mAdme SDK. 
		    pod 'madme_ios_sdk_framework/Debug+RSA', '~> 6.2.1'
		    # mAdme FCM wrapper (for mAdme SDK-driven FCM integration, if required). 
		    pod 'madme_ios_fcm_wrapper_framework/Debug', '~> 9.1.0'
		end

		target 'mAdme Sample Objc App' do
		    pods
		end

		post_install do |installer_representation|
		    installer_representation.pods_project.targets.each do |target|
		        target.build_configurations.each do |config|
		            config.build_settings['ONLY_ACTIVE_ARCH'] = 'NO'

                        config.build_settings['SWIFT_VERSION'] = '5.6'

		            config.build_settings['SWIFT_OPTIMIZATION_LEVEL'] = '-Osize'
		            config.build_settings['SWIFT_COMPILATION_MODE'] = 'wholemodule'

		            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'            
		        end
		    end
		end


5.  Run 'pod install' and make sure it runs without warnings or errors. 

6.  Enable push notifications in the project's capabilities.

7.  Enable the following background modes: 

		- Background fetch, 
		- Background processing, and 
		- Remote notifications. 


8.  Enable Access WiFi Information in the project's capabilities.  

9.  Enable Multipath in the project's capabilities (if required). If not required, disable the feature in MMConfiguration.plist. 

10.  Update Info.plist to include strings required by the alerts presented when the application needs access to the user's location. See also RequestWhenInUseAuthorization and RequestAlwaysAuthorization in the MMConfiguration sample below:

		- Privacy - Location Always Usage Description,
		- Privacy - Location When In Use Usage Description.
		- Privacy - Location Always and When In Use Usage Description.


11. Update Info.plist to include the following background task identifiers: 

		<key>BGTaskSchedulerPermittedIdentifiers</key>
		<array>
			<string>com.madme.bgRefreshTask</string>
			<string>com.madme.bgProcessingTask</string>
		</array>


12. Configure SDK to process mAdme background refresh tasks and background processing tasks as follows: 

    **Swift**

        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
            if #available(iOS 13.0, *) {                
                // Configure the SDK to process bg refresh tasks.
                MMSdk.configureBackgroundRefresh()
                
                // Configure the SDK to process bg processing tasks.
                MMSdk.configureBackgroundProcessing()
            }

            return true
        }

    **Objective-C**

        - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
            if (@available(iOS 13.0, *)) {
                // Configure the SDK to process bg refresh tasks.
                [MMSdk configureBackgroundRefresh];
                
                // Configure the SDK to process bg processing tasks.
                [MMSdk configureBackgroundProcessing];
            }

            return YES;
        }


13. Update Info.plist to allow for HTTP communication:

		<key>NSAppTransportSecurity</key>
		<dict>
		    <key>NSAllowsArbitraryLoads</key>
		    <true/>
		</dict>


14. Update Info.plist to white-list any applications that may be used for deep linking or checked if installed. For example: 

		<key>LSApplicationQueriesSchemes</key>
		<array>
		    <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
		    <string>fb</string>
		</array>


15. Update Info.plist with supported URL types. For example: 

		<key>CFBundleURLTypes</key>
		<array>
		    <dict>
		        <key>CFBundleTypeRole</key>
		        <string>None</string>
		        <key>CFBundleURLName</key>
		        <string>$(PRODUCT_BUNDLE_IDENTIFIER).url</string>
		        <key>CFBundleURLSchemes</key>
		        <array>
		            <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
		        </array>
		    </dict>
		</array>


16. If required, update Info.plist to add FirebaseAppDelegateProxyEnabled flag set to NO. 

17. Reference a module.modulemap file (if required) that includes the following: 

		module Firebase {
		    requires objc
		}


18. Reference a MMConfiguration.plist file (e.g., [MMConfiguration.plist](./MMConfiguration.plist)).  Madme will provide one based on the required configuration. If performing a production upgrade please double check with Madme before upgrading MMConfiguration.plist. See also the sample project(s) at https://bitbucket.org/madme/madme_ios_samples. 

19. Reference a Localizable.strings file. Madme will provide this. See the sample project at https://bitbucket.org/madme/madme_ios_samples. 

20. Reference a MMAssets.xcassets folder. Madme will provide this. See the sample project at https://bitbucket.org/madme/madme_ios_samples. 

21. For mAdme SDK-driven FCM integration (if required): 

    21.1. Link against madme_ios_fcm_wrapper_framework as per the above Podfile.  
    
    21.2. Update the application code as per the code excerpts below. For more information see the sample project(s) at https://bitbucket.org/madme/madme_ios_samples. FCM iOS documentation at https://firebase.google.com/docs/cloud-messaging/ios/client. 
    
    21.3. Disable the Firebase app delegate proxy in Info.plist. 
    
    21.4. If required, reference a module.modulemap file that contains the following code (may require updating the application project's HEADER_SEARCH_PATHS): 

		module Firebase {
		    requires objc
		}


22. Alternatively, for client application-driven FCM integration (if required): 

    22.1. Forward the FCM token to the mAdme SDK as follows: 

	**Swift** 

		func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
			// TODO: Application to retrieve the FCM token.
			let fcmTokenStr = "someFCMToken"

			// Forward the FCM token to the mAdme SDK.
			sdk.configureRemoteNotifications(with: fcmTokenStr)
		}

    **Objective-C**

		- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
			// TODO: Application to retrieve the FCM token. 
			NSString *fcmTokenStr = @"someFCMToken";

			// Forward the FCM token to the mAdme SDK. 
			[self.mAdmeSdk configureRemoteNotificationsWith: fcmTokenStr];
		}


23. Reference an rsa_public_key.der file. Madme will provide one based on the required configuration.

24. If required, reference a madme_cert.der file for certificate pinning. Madme will provide one based on the required configuration. Note that this feature can be enabled/disabled in MMConfiguration.plist.  

25. Add a DEBUG=1 preprocessor macro (if application written in Objective-C) or -D DEBUG other Swift flag (if application written in Swift) to the application's project/target Debug build configuration. Edit project.pbxproj directly. 

26. If required, add a APNS_PRODUCTION=1 preprocessor macro (if application written in Objective-C) or -D APNS_PRODUCTION other Swift flag (if application written in Swift) to the application's project/target Release/TestFlight/... build configuration. Edit project.pbxproj directly.  

27. If the application is written in Objective-C make sure that the Always Embed Swift Standard Libraries build setting is set to Yes.  

28. Set up the minimum background fetch interval. For example: 

	**Swift**

		func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
			// Set setMinimumBackgroundFetchInterval.
			application.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)

			return true
		}

	**Objective-C**

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		    // Set setMinimumBackgroundFetchInterval.
		    [application setMinimumBackgroundFetchInterval: UIApplicationBackgroundFetchIntervalMinimum];

		    return YES;
		}


29. Set up the mAdme SDK logging. For example: 

	**Swift**

		import madme_ios_sdk_shared

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
			// Configure logging. The following #if assumes a preprocessor macro "DEBUG=1" e.g., for all Debug targets.
			#if DEBUG
				MMLogger.type = .full
			#else
				MMLogger.type = .none
			#endif

			return true
		}

	**Objective-C**

		@import madme_ios_sdk_shared;

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		    // Configure logging. The following #if assumes a preprocessor macro "DEBUG=1" e.g., for all Debug targets.
		    #if DEBUG
		        [MMLogger setType: MMLoggerTypeFull];
		    #else
		        [MMLogger setType: MMLoggerTypeNone];
		    #endif

		    return YES;
		}


30. Initialize the mAdme SDK and set its delegate. Make sure to initialize the mAdme SDK before application(_:didFinishLaunchingWithOptions:) completes. For example: 

	**Swift**

		import madme_ios_sdk_sdk

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
			// Init the sdk and set its delegate.
			sdk.delegate = self
		}

		let sdk = MMSdk.sharedInstance

	**Objective-C**

		@import madme_ios_sdk;

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		    // Init and configure the mAdme SDK.
		    [self mAdmeSdk];

		    return YES;
		}

		@interface AppDelegate (mAdmeSdk)

		@property (nonatomic, readonly, copy) MMSdk * _Nonnull mAdmeSdk;

		@end

		@implementation AppDelegate (mAdmeSdk)
		    
		///
		/// MARK: mAdme SDK instance.
		///
		    
		- (MMSdk * _Nonnull)mAdmeSdk {
		    static MMSdk * _Nonnull mAdmeSdk;
		    
		    static dispatch_once_t onceToken;
		    
		    dispatch_once(&onceToken, ^{
		        // Get a reference to the mAdme SDK.
		        mAdmeSdk = MMSdk.sharedInstance;
		        
		        // Set self as delegate.
		        mAdmeSdk.delegate = self;
		    });
		    
		    return mAdmeSdk;
		}

		@end


31. Configure the oAuth2Id, oAuth2Secret and optional oClientIds. Madme will provide oAuth2Id and oAuth2Secret for your particular build configuration. For example: 

	**Swift**

		extension AppDelegate: MMSdkDelegateProtocol {

			var oAuth2Id: String {
			    return "eoc-ios-v1"
			}

			var oAuth2Secret: String {
			    return "30489gb4w9tv93q"
			}

			@objc var oClientIds: [String: String]? {
				return nil
			}    
		}

	**Objective-C**

		@interface AppDelegate (MMSdkDelegateProtocol) <MMSdkDelegateProtocol>
		    
		@property (nonatomic, readonly, copy) NSString * _Nonnull oAuth2Id;
		    
		@property (nonatomic, readonly, copy) NSString * _Nonnull oAuth2Secret;
		    
		@property (nonatomic, readonly, copy) NSDictionary<NSString *, NSString *> * _Nullable oClientIds;
		    
		@end

		@implementation AppDelegate (MMSdkDelegateProtocol)
		    
		- (NSString * _Nonnull)oAuth2Id {
		    return @"eoc-ios-v1";
		}
		    
		- (NSString * _Nonnull)oAuth2Secret {
		    return @"30489gb4w9tv93q";
		}
		    
		- (NSDictionary<NSString *, NSString *> * _Nullable)oClientIds {
		    return nil;
		}
		    
		@end


32. Forward local and remote notifications to the mAdme SDK. Check the SDK's response to know if these were mAdme notifications. For example: 

	**Swift**

		//
		// MARK: Notifications common. 
		//

		func process(notificationPayload: [AnyHashable: Any], oResultClosure: ((_ notificationProcessingResult: MMNotificationProcessingResultProtocol) -> Void)? = nil) {
			// Ask the SDK to process the notification.
			sdk.process(notificationPayload: notificationPayload) { notificationProcessingResult in
				switch notificationProcessingResult.state {
				case .notificationProcessedSucessfully:
					// TODO: ...

				case .notificationProcessedUnsuccessfully:
					// TODO: ...

				case .notificationNotProcessed:
					// TODO: ...

				@unknown default:
					// TODO: ...
				}

				// Finally, call the closure.
				oResultClosure?(notificationProcessingResult)
			}
		}

		///
		/// MARK: Managing local mAdme notifications.
		///

		func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
			// Set self as UNUserNotificationCenterDelegate delegate.
			UNUserNotificationCenter.current().delegate = self

			return true
		}

		extension AppDelegate: UNUserNotificationCenterDelegate {

			func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
				// Forward to the sdk.
				process(notificationPayload: response.notification.request.content.userInfo) { response in
				    // Call the completion handler.
				    completionHandler()
				}
			}
		}

		///
		/// MARK: Managing remote mAdme notifications. madme_ios_fcm_wrapper and fcmRemoteNotificationsConfigurationManager for mAdme SDK-driven FCM integration, if required.  
		///

		import madme_ios_fcm_wrapper

		func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
			// Decide whether to use the sandbox or production APNs.
			#if APNS_PRODUCTION
			let useProductionApns = true
			#else
			let useProductionApns = false
			#endif

			// Re-configure the SDK's notifications.
			MMFCMRemoteNotificationsConfigurationManager.sharedInstance.configureNotifications(deviceToken: deviceToken, useProductionApns: useProductionApns)
		}


		func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
			// TODO: ... 
		}

		func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
		    // Process the notification payload.
			process(notificationPayload: userInfo) { notificationProcessingResult in
				// Infer the handler's input from the notificationProcessingResult.
				switch notificationProcessingResult.state {
				case .notificationProcessedSucessfully:
					// Call the handler.
					completionHandler(.newData)

				case .notificationNotProcessed, .notificationProcessedUnsuccessfully:
					// Call the handler.
					completionHandler(.failed)

				@unknown default:
					// Call the handler.
					completionHandler(.failed)
				}
			}
		}

	**Objective-C**

		//
		// MARK: Notifications common. 
		//

		- (void)processNotificationPayload:(NSDictionary * _Nullable)notificationPayload fetchCompletionHandler:(void (^_Nullable)(UIBackgroundFetchResult result))completionHandler {
		    // Check that there is a payload to process.
		    if (notificationPayload) {
		        // Ask mAdme to process it.
		        [self.mAdmeSdk processWithNotificationPayload: notificationPayload oResultClosure:^(id<MMNotificationProcessingResultProtocol> _Nonnull notificationProcessingResult) {
		            switch (notificationProcessingResult.state) {
		                case MMNotificationProcessingResultStateNotificationProcessedSucessfully:
		                // TODO: ...
		                
		                // Call the handler.
		                completionHandler ? completionHandler(UIBackgroundFetchResultNewData) : nil;
		                
		                break;
		                
		                case MMNotificationProcessingResultStateNotificationProcessedUnsuccessfully:
		                // TODO: ...
		                
		                // Call the handler.
		                completionHandler ? completionHandler(UIBackgroundFetchResultFailed) : nil;
		                
		                break;
		                
		                case MMNotificationProcessingResultStateNotificationNotProcessed:
		                // TODO: ...
		                
		                // Call the handler.
		                completionHandler ? completionHandler(UIBackgroundFetchResultNoData) : nil;
		                
		                break;
		            }
		        }];
		    }
		}

		///
		/// MARK: Managing local mAdme notifications.
		///

		@implementation AppDelegate

		- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		    // Set self as UNUserNotificationCenterDelegate delegate.
		    [UNUserNotificationCenter currentNotificationCenter].delegate = self;

		    return YES;
		}

		@implementation AppDelegate (UNUserNotificationCenterDelegate)

		- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
		    // Process the notification payload.
		    [self processNotificationPayload: response.notification.request.content.userInfo fetchCompletionHandler: ^(UIBackgroundFetchResult result) {
		        // Call the completionHandler.
		        completionHandler();
		    }];
		}

		@end
		    
		///
		/// MARK: Managing remote mAdme notifications. madme_ios_fcm_wrapper and fcmRemoteNotificationsConfigurationManager for mAdme SDK-driven FCM integration, if required.  
		///

		@import madme_ios_fcm_wrapper;

		- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
		    // Decide whether to use the sandbox or production APNs.
		    #if DEBUG
		        BOOL useProductionApns = NO;
		    #else
		        BOOL useProductionApns = YES;
		    #endif
		    
		    // Re-configure the SDK's notifications.
		    [self.fcmRemoteNotificationsConfigurationManager configureNotificationsWithDeviceToken: deviceToken useProductionApns: useProductionApns];
		}
		    
		- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
		    // TODO: ...
		}
		    
		- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler: (void (^)(UIBackgroundFetchResult result))completionHandler {
		    // Process the notification payload.
		    [self processNotificationPayload: userInfo fetchCompletionHandler: completionHandler];
		}

		- (MMFCMRemoteNotificationsConfigurationManager * _Nonnull)fcmRemoteNotificationsConfigurationManager {
		    static MMFCMRemoteNotificationsConfigurationManager * _Nonnull fcmRemoteNotificationsConfigurationManager;

		    static dispatch_once_t onceToken;

		    dispatch_once(&onceToken, ^{
		        // Get a reference to the manager.
		        fcmRemoteNotificationsConfigurationManager = MMFCMRemoteNotificationsConfigurationManager.sharedInstance;
		    });

		    return fcmRemoteNotificationsConfigurationManager;
		}


33. Forward background URL session events to the mAdme SDK. For example: 

	**Swift**

		func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Swift.Void) {
			// Forward call to mAdme SDK.
			sdk.handleEventsForBackgroundURLSession(identifier: identifier) { _ in
				// TODO: ...

				// Call completionHandler.
				completionHandler()
			}
		}

	**Objective-C**

		- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)(void))completionHandler {
		    // Forward call to mAdme SDK.
		    [self.mAdmeSdk handleEventsForBackgroundURLSessionWithIdentifier: identifier resultClosure:^(id<MMUrlSessionEventsProcessingResultProtocol> _Nonnull urlSessionEventsProcessingResult) {
		        // TODO: ...
		        
		        // Call completionHandler.
		        completionHandler();
		    }];
		}


34. If required, forward open URL requests to the mAdme SDK. For example: 

	**Swift**

		func processUrl(url: URL) -> Bool {
			// Ask the MMSdk to process the url.
			return sdk.process(url: url) { urlProcessingResult in
				switch urlProcessingResult.state {
				case .urlNotProcessed:
					// TODO: ...
					break

				case .urlProcessedSucessfully:
					// TODO: ...
					break

				case .urlProcessedUnsuccessfully:
					// TODO: ...
					break

				@unknown default:
					// TODO: ...
					break
				}
			}
		}

		@objc(application:openURL:options:) func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
			// Process the url.
			return processUrl(url: url)
		}

	**Objective-C**

		- (BOOL)processUrl:(NSURL *)url {
		    // Ask MMSdk to process the url.
		    return [self.mAdmeSdk processWithUrl: url oResultClosure:^(id<MMUrlProcessingResultProtocol> _Nonnull urlProcessingResult) {
		        switch (urlProcessingResult.state) {
		            case MMUrlProcessingResultStateUrlProcessedSucessfully:
		            // TODO: ...
		            break;
		            
		            case MMUrlProcessingResultStateUrlProcessedUnsuccessfully:
		            // TODO: ...
		            break;
		            
		            case MMUrlProcessingResultStateUrlNotProcessed:
		            // TODO: ...
		            break;
		        }
		    }];
		}

		- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
		    // Process the url.
		    return [self processUrl: url];
		}


35. Forward background fetch calls to the mAdme SDK. For example: 

	**Swift**
    
		func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
			// Forward call to MMSdk.
			sdk.application(application, performFetchWithCompletionHandler: completionHandler)
		}

	**Objective-C**

		- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
		    // Forward call to MMSdk.
		    [self.mAdmeSdk application: application performFetchWithCompletionHandler: completionHandler];
		}


36. Optionally update MMConfiguration.plist's scheduler time intervals, UI colors, border widths, etc.    

37. Sample code of using the mAdme SDK to present an In-App campaign. 

	**Swift**

		import madme_ios_sdk

		final class ApplicationViewController: UIViewController {

			override func viewDidLoad() {
				super.viewDidLoad()

				// Some time later... Delay here just for demo purposes.
				DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
					MMSdk.sharedInstance.presentAd(tags: ["SAMPLE_TAG"], tagsAggregateQualifier: .any, adPresentationMode: .direct, oParentViewController: nil) { adPresentationResult in
						// Direct flow based on the adPresentationResult's state.
						switch adPresentationResult.state {
						case .adNotPresented:
							// TODO: ...
							break

						case .adViewed:
							// TODO: ...
							break

						case .adTapped:
							// TODO: ...
							break

						case .adViewedAndTapped:
							// TODO: ...
							break

						case .adDismissed:
							// TODO: ...
							break

						case .adNotified:
							// TODO: ...
							break

						case .adNotifyFailed:
							// TODO: ...
							break

						@unknown default:
							// TODO: ...
							break
						}
					}
				}
			}
		}

	**Objective-C**

		#import "ViewController.h"
		#import "AppDelegate.h"

		@import madme_ios_sdk;

		@interface ViewController ()

		@end

		@implementation ViewController

		- (void)viewDidLoad {
		    [super viewDidLoad];
		    
		    // Some time later... Delay here just for demo purposes.
		    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		        // Ask the mAdmeSdk to present an ad with a specific tag.
		        [((AppDelegate *)(UIApplication.sharedApplication.delegate)).mAdmeSdk presentAdWithTags: [NSSet setWithObject: @"SAMPLE_TAG"] tagsAggregateQualifier: MMAggregateQualifierAny adPresentationMode: MMAdPresentationModeDirect oParentViewController: nil oResultClosure: ^(id<MMAdPresentationResultProtocol> _Nonnull adPresentationResult) {
		            // Direct flow based on the adPresentationResult's state.
		            switch (adPresentationResult.state) {
		                case MMAdPresentationResultStateAdNotPresented:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdViewed:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdTapped:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdViewedAndTapped:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdDismissed:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdNotified:
		                    // TODO: ...
		                    break;
		                    
		                case MMAdPresentationResultStateAdNotifyFailed:
		                    // TODO: ...
		                    break;
		            }
		        }];
		    });
		}

		@end


38. Client apps can receive up-to-date information on the viewed campaigns (surveys and rating campaigns excluded) by observing NSNotification.Campaigns as per the code below: 

	**Swift**
        
		// Observe .Campaigns notifications.
		_ = NotificationCenter.default.addObserver(forName: .Campaigns, object: MMSdk.sharedInstance, queue: nil) { notification in
			// Get the campaigns (if any).
			guard let campaigns = notification.userInfo?[MMSdk.CampaignsKey] else {
				// TODO: ...
				return
			}

			// TODO: ...
		}

	**Objective-C**

		// Observe NSNotification.Campaigns notifications.
		... = [NSNotificationCenter.defaultCenter addObserverForName: NSNotification.Campaigns object: mAdmeSdk queue: nil usingBlock: ^(NSNotification * _Nonnull notification) {
		    // Get the campaigns (if any).
		    NSArray<MMCampaign *> *campaigns = notification.userInfo[MMSdk.CampaignsKey];
		    
		    // Log.
		    NSLog(@"Got %lu campaigns.", (unsigned long)campaigns.count);
		}];


39. For more information see the API docs at https://bitbucket.org/madme/madme_ios_sdk_framework5/src/master/madme_ios_sdk_api_docs/index.html. 
