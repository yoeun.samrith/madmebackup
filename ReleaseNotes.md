# Release Notes #

## 6.2.1 ##

* https://mad-me.atlassian.net/browse/IOS-1595

* https://mad-me.atlassian.net/browse/IOS-1594

* https://mad-me.atlassian.net/browse/IOS-1593

* https://mad-me.atlassian.net/browse/IOS-1592

* https://mad-me.atlassian.net/browse/IOS-1591

* https://mad-me.atlassian.net/browse/IOS-1590

* https://mad-me.atlassian.net/browse/IOS-1589

* https://mad-me.atlassian.net/browse/IOS-1588

* https://mad-me.atlassian.net/browse/IOS-1587

* https://mad-me.atlassian.net/browse/IOS-1586

* https://mad-me.atlassian.net/browse/IOS-1585

* https://mad-me.atlassian.net/browse/IOS-1584

* https://mad-me.atlassian.net/browse/IOS-1583

## 6.2.0 ##

* https://mad-me.atlassian.net/browse/IOS-1576

* https://mad-me.atlassian.net/browse/IOS-1575

* https://mad-me.atlassian.net/browse/IOS-1574

* https://mad-me.atlassian.net/browse/IOS-1573

* https://mad-me.atlassian.net/browse/IOS-1572

* https://mad-me.atlassian.net/browse/IOS-1571

* https://mad-me.atlassian.net/browse/IOS-1567

* https://mad-me.atlassian.net/browse/IOS-1566

* https://mad-me.atlassian.net/browse/IOS-1565

* https://mad-me.atlassian.net/browse/IOS-1564

* https://mad-me.atlassian.net/browse/IOS-1563

* https://mad-me.atlassian.net/browse/IOS-1561

* https://mad-me.atlassian.net/browse/IOS-1560

* https://mad-me.atlassian.net/browse/IOS-1559

* https://mad-me.atlassian.net/browse/IOS-1558

* https://mad-me.atlassian.net/browse/IOS-1557

## 6.1.1 ##

* https://mad-me.atlassian.net/browse/IOS-1553

* https://mad-me.atlassian.net/browse/IOS-1552

* https://mad-me.atlassian.net/browse/IOS-1551

* https://mad-me.atlassian.net/browse/IOS-1550

* https://mad-me.atlassian.net/browse/IOS-1549

* https://mad-me.atlassian.net/browse/IOS-1548

* https://mad-me.atlassian.net/browse/IOS-1547

* https://mad-me.atlassian.net/browse/IOS-1546

* https://mad-me.atlassian.net/browse/IOS-1544

* https://mad-me.atlassian.net/browse/IOS-1543

* https://mad-me.atlassian.net/browse/IOS-1542

## 6.1.0 ##

* Xcode 13.0/Swift 5.5/iOS 15.0 SDK/iOS 11.0+

* https://mad-me.atlassian.net/browse/IOS-1537

* https://mad-me.atlassian.net/browse/IOS-1536

* https://mad-me.atlassian.net/browse/IOS-1535

* https://mad-me.atlassian.net/browse/IOS-1534

* https://mad-me.atlassian.net/browse/IOS-1533

* https://mad-me.atlassian.net/browse/IOS-1532

* https://mad-me.atlassian.net/browse/IOS-1531

* https://mad-me.atlassian.net/browse/IOS-1530

* https://mad-me.atlassian.net/browse/IOS-1529

* https://mad-me.atlassian.net/browse/IOS-1525

* https://mad-me.atlassian.net/browse/IOS-1524

* https://mad-me.atlassian.net/browse/IOS-1523

* https://mad-me.atlassian.net/browse/IOS-1522

* https://mad-me.atlassian.net/browse/IOS-1521

* https://mad-me.atlassian.net/browse/IOS-1520

* https://mad-me.atlassian.net/browse/IOS-1519

* https://mad-me.atlassian.net/browse/IOS-1518

* https://mad-me.atlassian.net/browse/IOS-1517

* https://mad-me.atlassian.net/browse/IOS-1516

* https://mad-me.atlassian.net/browse/IOS-1515

## 6.0.3 ##

* https://mad-me.atlassian.net/browse/IOS-1510

* https://mad-me.atlassian.net/browse/IOS-1509

* https://mad-me.atlassian.net/browse/IOS-1508

## 6.0.2 ##

* https://mad-me.atlassian.net/browse/IOS-1504

* https://mad-me.atlassian.net/browse/IOS-1503

* https://mad-me.atlassian.net/browse/IOS-1501

* https://mad-me.atlassian.net/browse/IOS-1500

* https://mad-me.atlassian.net/browse/IOS-1499

* https://mad-me.atlassian.net/browse/IOS-1498

* https://mad-me.atlassian.net/browse/IOS-1497

* https://mad-me.atlassian.net/browse/IOS-1496

* https://mad-me.atlassian.net/browse/IOS-1494

* https://mad-me.atlassian.net/browse/IOS-1493

* https://mad-me.atlassian.net/browse/IOS-1492

## 6.0.1 ##

* https://mad-me.atlassian.net/browse/IOS-1489

* https://mad-me.atlassian.net/browse/IOS-1488

* https://mad-me.atlassian.net/browse/IOS-1487

* https://mad-me.atlassian.net/browse/IOS-1486

* https://mad-me.atlassian.net/browse/IOS-1485

* https://mad-me.atlassian.net/browse/IOS-1483

## 6.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-1480

* https://mad-me.atlassian.net/browse/IOS-1479

* https://mad-me.atlassian.net/browse/IOS-1478

* https://mad-me.atlassian.net/browse/IOS-1477

* https://mad-me.atlassian.net/browse/IOS-1472

* https://mad-me.atlassian.net/browse/IOS-1471

* https://mad-me.atlassian.net/browse/IOS-1470

* https://mad-me.atlassian.net/browse/IOS-1469

* https://mad-me.atlassian.net/browse/IOS-1468

* https://mad-me.atlassian.net/browse/IOS-1467

* https://mad-me.atlassian.net/browse/IOS-1466

* https://mad-me.atlassian.net/browse/IOS-1465

* https://mad-me.atlassian.net/browse/IOS-1464

* https://mad-me.atlassian.net/browse/IOS-1463

* https://mad-me.atlassian.net/browse/IOS-1462

* https://mad-me.atlassian.net/browse/IOS-1461

* https://mad-me.atlassian.net/browse/IOS-1459

* https://mad-me.atlassian.net/browse/IOS-1458

* https://mad-me.atlassian.net/browse/IOS-1456

* https://mad-me.atlassian.net/browse/IOS-1455

* https://mad-me.atlassian.net/browse/IOS-1454

* https://mad-me.atlassian.net/browse/IOS-1452

* https://mad-me.atlassian.net/browse/IOS-1446

## 5.3.1 ##

* https://mad-me.atlassian.net/browse/IOS-1473

## 5.3.0 ##

* https://mad-me.atlassian.net/browse/IOS-1441

* https://mad-me.atlassian.net/browse/IOS-1440

* https://mad-me.atlassian.net/browse/IOS-1439

* https://mad-me.atlassian.net/browse/IOS-1438

* https://mad-me.atlassian.net/browse/IOS-1437

* https://mad-me.atlassian.net/browse/IOS-1436

* https://mad-me.atlassian.net/browse/IOS-1435

* https://mad-me.atlassian.net/browse/IOS-1434

* https://mad-me.atlassian.net/browse/IOS-1433

* https://mad-me.atlassian.net/browse/IOS-1432

* https://mad-me.atlassian.net/browse/IOS-1431

* https://mad-me.atlassian.net/browse/IOS-1430

## 5.2.0 ##

* https://mad-me.atlassian.net/browse/IOS-1419

* https://mad-me.atlassian.net/browse/IOS-1418

* https://mad-me.atlassian.net/browse/IOS-1417

* https://mad-me.atlassian.net/browse/IOS-1416

* https://mad-me.atlassian.net/browse/IOS-1415

* https://mad-me.atlassian.net/browse/IOS-1414

* https://mad-me.atlassian.net/browse/IOS-1413

* https://mad-me.atlassian.net/browse/IOS-1412

* https://mad-me.atlassian.net/browse/IOS-1411

* https://mad-me.atlassian.net/browse/IOS-1410

* https://mad-me.atlassian.net/browse/IOS-1406

* https://mad-me.atlassian.net/browse/IOS-1333

## 5.1.1 ##

* https://mad-me.atlassian.net/browse/IOS-1407

* https://mad-me.atlassian.net/browse/IOS-1406

* https://mad-me.atlassian.net/browse/IOS-1405

## 5.1.0 ##

* https://mad-me.atlassian.net/browse/IOS-1400

* https://mad-me.atlassian.net/browse/IOS-1399

## 5.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-1391

* https://mad-me.atlassian.net/browse/IOS-1390

* https://mad-me.atlassian.net/browse/IOS-1388

* https://mad-me.atlassian.net/browse/IOS-1387

* https://mad-me.atlassian.net/browse/IOS-1386

* https://mad-me.atlassian.net/browse/IOS-1385

* https://mad-me.atlassian.net/browse/IOS-1384

* https://mad-me.atlassian.net/browse/IOS-1383

* https://mad-me.atlassian.net/browse/IOS-1382

* https://mad-me.atlassian.net/browse/IOS-1381

* https://mad-me.atlassian.net/browse/IOS-1380

* https://mad-me.atlassian.net/browse/IOS-1379

* https://mad-me.atlassian.net/browse/IOS-1378

* https://mad-me.atlassian.net/browse/IOS-1377

* https://mad-me.atlassian.net/browse/IOS-1376

* https://mad-me.atlassian.net/browse/IOS-1375

* https://mad-me.atlassian.net/browse/IOS-1374

* https://mad-me.atlassian.net/browse/IOS-1369

* https://mad-me.atlassian.net/browse/IOS-1368

## 5.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-1391

* https://mad-me.atlassian.net/browse/IOS-1390

* https://mad-me.atlassian.net/browse/IOS-1388

* https://mad-me.atlassian.net/browse/IOS-1387

* https://mad-me.atlassian.net/browse/IOS-1386

* https://mad-me.atlassian.net/browse/IOS-1385

* https://mad-me.atlassian.net/browse/IOS-1384

* https://mad-me.atlassian.net/browse/IOS-1383

* https://mad-me.atlassian.net/browse/IOS-1382

* https://mad-me.atlassian.net/browse/IOS-1381

* https://mad-me.atlassian.net/browse/IOS-1380

* https://mad-me.atlassian.net/browse/IOS-1379

* https://mad-me.atlassian.net/browse/IOS-1378

* https://mad-me.atlassian.net/browse/IOS-1377

* https://mad-me.atlassian.net/browse/IOS-1376

* https://mad-me.atlassian.net/browse/IOS-1375

* https://mad-me.atlassian.net/browse/IOS-1374

* https://mad-me.atlassian.net/browse/IOS-1369

* https://mad-me.atlassian.net/browse/IOS-1368

## 4.6.0 ##

* https://mad-me.atlassian.net/browse/IOS-1361

* https://mad-me.atlassian.net/browse/IOS-1360

* https://mad-me.atlassian.net/browse/IOS-1358

* https://mad-me.atlassian.net/browse/IOS-1357

* https://mad-me.atlassian.net/browse/IOS-1356

* https://mad-me.atlassian.net/browse/IOS-1355

* https://mad-me.atlassian.net/browse/IOS-1354

* https://mad-me.atlassian.net/browse/IOS-1353

* https://mad-me.atlassian.net/browse/IOS-1352

* https://mad-me.atlassian.net/browse/IOS-1351

## 4.5.3 ##

* https://mad-me.atlassian.net/browse/IOS-1347

* https://mad-me.atlassian.net/browse/IOS-1345

* https://mad-me.atlassian.net/browse/IOS-1344

* https://mad-me.atlassian.net/browse/IOS-1343

* https://mad-me.atlassian.net/browse/IOS-1342

* https://mad-me.atlassian.net/browse/IOS-1341

* https://mad-me.atlassian.net/browse/IOS-1340

## 4.5.2 ##

* https://mad-me.atlassian.net/browse/IOS-1334

* https://mad-me.atlassian.net/browse/IOS-1332

* https://mad-me.atlassian.net/browse/IOS-1331

* https://mad-me.atlassian.net/browse/IOS-1330

* https://mad-me.atlassian.net/browse/IOS-1329

* https://mad-me.atlassian.net/browse/IOS-1328

* https://mad-me.atlassian.net/browse/IOS-1327

## 4.5.1 ##

* Xcode 11.5/Swift 5.2.4/iOS 13.5 and cocoapods 1.9.0. 

* https://mad-me.atlassian.net/browse/IOS-1324

* https://mad-me.atlassian.net/browse/IOS-1322

## 4.5.0 ##

* https://mad-me.atlassian.net/browse/IOS-1305

* https://mad-me.atlassian.net/browse/IOS-1283

## 4.4.0 ##

* https://mad-me.atlassian.net/browse/IOS-1299

* https://mad-me.atlassian.net/browse/IOS-1298

* https://mad-me.atlassian.net/browse/IOS-1297

* https://mad-me.atlassian.net/browse/IOS-1296

* https://mad-me.atlassian.net/browse/IOS-1295

* https://mad-me.atlassian.net/browse/IOS-1294

* https://mad-me.atlassian.net/browse/IOS-1293

* https://mad-me.atlassian.net/browse/IOS-1284

* https://mad-me.atlassian.net/browse/IOS-1282

* https://mad-me.atlassian.net/browse/IOS-1281

* https://mad-me.atlassian.net/browse/IOS-1280

* https://mad-me.atlassian.net/browse/IOS-1278

* https://mad-me.atlassian.net/browse/IOS-1277

* https://mad-me.atlassian.net/browse/IOS-1276

## 4.3.0 ##

* https://mad-me.atlassian.net/browse/IOS-1265

* https://mad-me.atlassian.net/browse/IOS-1264

* https://mad-me.atlassian.net/browse/IOS-1263

* https://mad-me.atlassian.net/browse/IOS-1262

* https://mad-me.atlassian.net/browse/IOS-1261

* https://mad-me.atlassian.net/browse/IOS-1258

* https://mad-me.atlassian.net/browse/IOS-1257

* https://mad-me.atlassian.net/browse/IOS-1256

* https://mad-me.atlassian.net/browse/IOS-1254

* https://mad-me.atlassian.net/browse/IOS-1253

* https://mad-me.atlassian.net/browse/IOS-1252

* https://mad-me.atlassian.net/browse/IOS-1251

* https://mad-me.atlassian.net/browse/IOS-1250

* https://mad-me.atlassian.net/browse/IOS-1249

* https://mad-me.atlassian.net/browse/IOS-1248

* https://mad-me.atlassian.net/browse/IOS-1247

* https://mad-me.atlassian.net/browse/IOS-1246

* https://mad-me.atlassian.net/browse/IOS-1244

* https://mad-me.atlassian.net/browse/IOS-1243

* https://mad-me.atlassian.net/browse/IOS-1242

* https://mad-me.atlassian.net/browse/IOS-1241

* https://mad-me.atlassian.net/browse/IOS-1240

## 4.2.4 ##

* Minor internal changes. 

## 4.2.3 ##

* https://mad-me.atlassian.net/browse/IOS-1234

* https://mad-me.atlassian.net/browse/IOS-1233

* https://mad-me.atlassian.net/browse/IOS-1232

* https://mad-me.atlassian.net/browse/IOS-1231

* https://mad-me.atlassian.net/browse/IOS-1230

* https://mad-me.atlassian.net/browse/IOS-1229

* https://mad-me.atlassian.net/browse/IOS-1228

* https://mad-me.atlassian.net/browse/IOS-1227

* https://mad-me.atlassian.net/browse/IOS-1226

* https://mad-me.atlassian.net/browse/IOS-1225

* https://mad-me.atlassian.net/browse/IOS-1224

* https://mad-me.atlassian.net/browse/IOS-1223

* https://mad-me.atlassian.net/browse/IOS-1222

* https://mad-me.atlassian.net/browse/IOS-1221

* https://mad-me.atlassian.net/browse/IOS-1220

* https://mad-me.atlassian.net/browse/IOS-1219

* https://mad-me.atlassian.net/browse/IOS-1218

* https://mad-me.atlassian.net/browse/IOS-1217

* https://mad-me.atlassian.net/browse/IOS-1216

* https://mad-me.atlassian.net/browse/IOS-1215

* https://mad-me.atlassian.net/browse/IOS-1214

* https://mad-me.atlassian.net/browse/IOS-1213

* https://mad-me.atlassian.net/browse/IOS-1212

* https://mad-me.atlassian.net/browse/IOS-1211

* https://mad-me.atlassian.net/browse/IOS-1210

* https://mad-me.atlassian.net/browse/IOS-1209

* https://mad-me.atlassian.net/browse/IOS-1208

* https://mad-me.atlassian.net/browse/IOS-1206

* https://mad-me.atlassian.net/browse/IOS-1205

* https://mad-me.atlassian.net/browse/IOS-1204

* https://mad-me.atlassian.net/browse/IOS-1203

* https://mad-me.atlassian.net/browse/IOS-1202

* https://mad-me.atlassian.net/browse/IOS-1201

* https://mad-me.atlassian.net/browse/IOS-1200

* https://mad-me.atlassian.net/browse/IOS-1199

* https://mad-me.atlassian.net/browse/IOS-1198

* https://mad-me.atlassian.net/browse/IOS-1197

* https://mad-me.atlassian.net/browse/IOS-1196

* https://mad-me.atlassian.net/browse/IOS-1195

* https://mad-me.atlassian.net/browse/IOS-1194

* https://mad-me.atlassian.net/browse/IOS-1185

## 4.2.2 ##

* https://mad-me.atlassian.net/browse/IOS-1186

* https://mad-me.atlassian.net/browse/IOS-1184

* https://mad-me.atlassian.net/browse/IOS-1183

* https://mad-me.atlassian.net/browse/IOS-1182

* https://mad-me.atlassian.net/browse/IOS-1181

* https://mad-me.atlassian.net/browse/IOS-1180

* https://mad-me.atlassian.net/browse/IOS-1179

* https://mad-me.atlassian.net/browse/IOS-1178

* https://mad-me.atlassian.net/browse/IOS-686

## 4.2.1 ##

* https://mad-me.atlassian.net/browse/IOS-1173

* https://mad-me.atlassian.net/browse/IOS-1172

* https://mad-me.atlassian.net/browse/IOS-1170

* https://mad-me.atlassian.net/browse/IOS-1169

* https://mad-me.atlassian.net/browse/IOS-1168

* https://mad-me.atlassian.net/browse/IOS-1167

* https://mad-me.atlassian.net/browse/IOS-1166

* https://mad-me.atlassian.net/browse/IOS-1165

* https://mad-me.atlassian.net/browse/IOS-1164

* https://mad-me.atlassian.net/browse/IOS-1162

* https://mad-me.atlassian.net/browse/IOS-1161

* https://mad-me.atlassian.net/browse/IOS-1160

* https://mad-me.atlassian.net/browse/IOS-1159

* https://mad-me.atlassian.net/browse/IOS-1158

* https://mad-me.atlassian.net/browse/IOS-1157

* https://mad-me.atlassian.net/browse/IOS-1156

* https://mad-me.atlassian.net/browse/IOS-1154

* https://mad-me.atlassian.net/browse/IOS-1153

* https://mad-me.atlassian.net/browse/IOS-1152

* https://mad-me.atlassian.net/browse/IOS-1151

* https://mad-me.atlassian.net/browse/IOS-1150

* https://mad-me.atlassian.net/browse/IOS-1149

* https://mad-me.atlassian.net/browse/IOS-1148

* https://mad-me.atlassian.net/browse/IOS-1147

* https://mad-me.atlassian.net/browse/IOS-1146

* https://mad-me.atlassian.net/browse/IOS-1145

* https://mad-me.atlassian.net/browse/IOS-1144

* https://mad-me.atlassian.net/browse/IOS-1143

* https://mad-me.atlassian.net/browse/IOS-1142

* https://mad-me.atlassian.net/browse/IOS-1141

* https://mad-me.atlassian.net/browse/IOS-1139

* https://mad-me.atlassian.net/browse/IOS-1137

* https://mad-me.atlassian.net/browse/IOS-1136

* https://mad-me.atlassian.net/browse/IOS-1135

* https://mad-me.atlassian.net/browse/IOS-1134

* https://mad-me.atlassian.net/browse/IOS-1133

## 4.2.0 ##

* https://mad-me.atlassian.net/browse/IOS-1127

* https://mad-me.atlassian.net/browse/IOS-1125

* https://mad-me.atlassian.net/browse/IOS-1124

* https://mad-me.atlassian.net/browse/IOS-1123

* https://mad-me.atlassian.net/browse/IOS-1122

* https://mad-me.atlassian.net/browse/IOS-1119

* https://mad-me.atlassian.net/browse/IOS-1118

* https://mad-me.atlassian.net/browse/IOS-1117

* https://mad-me.atlassian.net/browse/IOS-1116

* https://mad-me.atlassian.net/browse/IOS-1115

* https://mad-me.atlassian.net/browse/IOS-681

## 4.1.0 ##

* https://mad-me.atlassian.net/browse/IOS-1093

* https://mad-me.atlassian.net/browse/IOS-1091

* https://mad-me.atlassian.net/browse/IOS-1090

* https://mad-me.atlassian.net/browse/IOS-1089

* https://mad-me.atlassian.net/browse/IOS-1088

* https://mad-me.atlassian.net/browse/IOS-1087

* https://mad-me.atlassian.net/browse/IOS-1086

* https://mad-me.atlassian.net/browse/IOS-1085

* https://mad-me.atlassian.net/browse/IOS-1084

* https://mad-me.atlassian.net/browse/IOS-1083

* https://mad-me.atlassian.net/browse/IOS-1082

* https://mad-me.atlassian.net/browse/IOS-1081

* https://mad-me.atlassian.net/browse/IOS-1080

* https://mad-me.atlassian.net/browse/IOS-1076

* https://mad-me.atlassian.net/browse/IOS-1057

* https://mad-me.atlassian.net/browse/IOS-1056

* https://mad-me.atlassian.net/browse/IOS-910

* https://mad-me.atlassian.net/browse/IOS-909

## 4.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-1071

* https://mad-me.atlassian.net/browse/IOS-1070

* https://mad-me.atlassian.net/browse/IOS-1069

* https://mad-me.atlassian.net/browse/IOS-1068

* https://mad-me.atlassian.net/browse/IOS-1064

* https://mad-me.atlassian.net/browse/IOS-1063

* https://mad-me.atlassian.net/browse/IOS-1062

* https://mad-me.atlassian.net/browse/IOS-1061

* https://mad-me.atlassian.net/browse/IOS-1060

* https://mad-me.atlassian.net/browse/IOS-1059

* https://mad-me.atlassian.net/browse/IOS-1058

## 3.2.0 ##

* https://mad-me.atlassian.net/browse/IOS-1050

* https://mad-me.atlassian.net/browse/IOS-1049

* https://mad-me.atlassian.net/browse/IOS-1048

* https://mad-me.atlassian.net/browse/IOS-1047

* https://mad-me.atlassian.net/browse/IOS-1045

* https://mad-me.atlassian.net/browse/IOS-1044

* https://mad-me.atlassian.net/browse/IOS-1041

* https://mad-me.atlassian.net/browse/IOS-1039

* https://mad-me.atlassian.net/browse/IOS-1038

* https://mad-me.atlassian.net/browse/IOS-1036

* https://mad-me.atlassian.net/browse/IOS-1035

* https://mad-me.atlassian.net/browse/IOS-1031

* https://mad-me.atlassian.net/browse/IOS-1030

* https://mad-me.atlassian.net/browse/IOS-1028

* https://mad-me.atlassian.net/browse/IOS-1027

* https://mad-me.atlassian.net/browse/IOS-1026

* https://mad-me.atlassian.net/browse/IOS-1025

* https://mad-me.atlassian.net/browse/IOS-1024

* https://mad-me.atlassian.net/browse/IOS-1023

* https://mad-me.atlassian.net/browse/IOS-1022

* https://mad-me.atlassian.net/browse/IOS-1020

* https://mad-me.atlassian.net/browse/IOS-605

## 3.1.5 ##

* https://mad-me.atlassian.net/browse/IOS-1012

* https://mad-me.atlassian.net/browse/IOS-1011

## 3.1.4 ##

* https://mad-me.atlassian.net/browse/IOS-1007

* https://mad-me.atlassian.net/browse/IOS-1005

* https://mad-me.atlassian.net/browse/IOS-1004

* https://mad-me.atlassian.net/browse/IOS-1001

* https://mad-me.atlassian.net/browse/IOS-1000

* https://mad-me.atlassian.net/browse/IOS-999

* https://mad-me.atlassian.net/browse/IOS-998

* https://mad-me.atlassian.net/browse/IOS-996

* https://mad-me.atlassian.net/browse/IOS-995

* https://mad-me.atlassian.net/browse/IOS-994

## 3.1.3 ##

* https://mad-me.atlassian.net/browse/IOS-990

## 3.1.2 ##

* https://mad-me.atlassian.net/browse/IOS-986

## 3.1.1 ##

* https://mad-me.atlassian.net/browse/IOS-981

* https://mad-me.atlassian.net/browse/IOS-979

* https://mad-me.atlassian.net/browse/IOS-978

* https://mad-me.atlassian.net/browse/IOS-977

* https://mad-me.atlassian.net/browse/IOS-976

* https://mad-me.atlassian.net/browse/IOS-954

## 3.1.0 ##

* https://mad-me.atlassian.net/browse/IOS-974

* https://mad-me.atlassian.net/browse/IOS-972

* https://mad-me.atlassian.net/browse/IOS-971

* https://mad-me.atlassian.net/browse/IOS-970

* https://mad-me.atlassian.net/browse/IOS-969

* https://mad-me.atlassian.net/browse/IOS-968

* https://mad-me.atlassian.net/browse/IOS-967

* https://mad-me.atlassian.net/browse/IOS-966

* https://mad-me.atlassian.net/browse/IOS-965

* https://mad-me.atlassian.net/browse/IOS-964

* https://mad-me.atlassian.net/browse/IOS-963

* https://mad-me.atlassian.net/browse/IOS-962

* https://mad-me.atlassian.net/browse/IOS-961

## 3.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-959

* https://mad-me.atlassian.net/browse/IOS-958

* https://mad-me.atlassian.net/browse/IOS-957

* https://mad-me.atlassian.net/browse/IOS-956

* https://mad-me.atlassian.net/browse/IOS-955

* https://mad-me.atlassian.net/browse/IOS-952

* https://mad-me.atlassian.net/browse/IOS-951

* https://mad-me.atlassian.net/browse/IOS-935

## 2.10.2 ##

* https://mad-me.atlassian.net/browse/IOS-947

* https://mad-me.atlassian.net/browse/IOS-946

* https://mad-me.atlassian.net/browse/IOS-945

* https://mad-me.atlassian.net/browse/IOS-944

* https://mad-me.atlassian.net/browse/IOS-943

* https://mad-me.atlassian.net/browse/IOS-941

* https://mad-me.atlassian.net/browse/IOS-940

* https://mad-me.atlassian.net/browse/IOS-939

* https://mad-me.atlassian.net/browse/IOS-938

* https://mad-me.atlassian.net/browse/IOS-937

* https://mad-me.atlassian.net/browse/IOS-934

* https://mad-me.atlassian.net/browse/IOS-933

* https://mad-me.atlassian.net/browse/IOS-932

* https://mad-me.atlassian.net/browse/IOS-931

* https://mad-me.atlassian.net/browse/IOS-930

* https://mad-me.atlassian.net/browse/IOS-929

* https://mad-me.atlassian.net/browse/IOS-927

* https://mad-me.atlassian.net/browse/IOS-926

## 2.10.1 ##

* https://mad-me.atlassian.net/browse/IOS-916

* https://mad-me.atlassian.net/browse/IOS-915

## 2.10.0 ##

* https://mad-me.atlassian.net/browse/IOS-908

* https://mad-me.atlassian.net/browse/IOS-905

* https://mad-me.atlassian.net/browse/IOS-892

* https://mad-me.atlassian.net/browse/IOS-891

* https://mad-me.atlassian.net/browse/IOS-890

* https://mad-me.atlassian.net/browse/IOS-889

* https://mad-me.atlassian.net/browse/IOS-888

* https://mad-me.atlassian.net/browse/IOS-887

* https://mad-me.atlassian.net/browse/IOS-886

* https://mad-me.atlassian.net/browse/IOS-885

* https://mad-me.atlassian.net/browse/IOS-800

## 2.9.3 ##

* https://mad-me.atlassian.net/browse/IOS-900

* https://mad-me.atlassian.net/browse/IOS-899

* https://mad-me.atlassian.net/browse/IOS-898

## 2.9.2 ##

* https://mad-me.atlassian.net/browse/IOS-893

* https://mad-me.atlassian.net/browse/IOS-892

* https://mad-me.atlassian.net/browse/IOS-891

* https://mad-me.atlassian.net/browse/IOS-890

## 2.9.1 ##

* https://mad-me.atlassian.net/browse/IOS-879

* https://mad-me.atlassian.net/browse/IOS-877

## 2.9.0 ##

* https://mad-me.atlassian.net/browse/IOS-864

## 2.8.2 ##

* https://mad-me.atlassian.net/browse/IOS-860

* https://mad-me.atlassian.net/browse/IOS-858

## 2.8.1 ##

* https://mad-me.atlassian.net/browse/IOS-846

* https://mad-me.atlassian.net/browse/IOS-845

* https://mad-me.atlassian.net/browse/IOS-844

* https://mad-me.atlassian.net/browse/IOS-843

* https://mad-me.atlassian.net/browse/IOS-842

## 2.8.0 ##

* https://mad-me.atlassian.net/browse/IOS-825

* https://mad-me.atlassian.net/browse/IOS-824

* https://mad-me.atlassian.net/browse/IOS-823

* https://mad-me.atlassian.net/browse/IOS-822

* https://mad-me.atlassian.net/browse/IOS-821

* https://mad-me.atlassian.net/browse/IOS-820

* https://mad-me.atlassian.net/browse/IOS-819

* https://mad-me.atlassian.net/browse/IOS-818

* https://mad-me.atlassian.net/browse/IOS-817

* https://mad-me.atlassian.net/browse/IOS-816

* https://mad-me.atlassian.net/browse/IOS-815

* https://mad-me.atlassian.net/browse/IOS-813

* https://mad-me.atlassian.net/browse/IOS-812

## 2.7.9 ##

* https://mad-me.atlassian.net/browse/IOS-786

## 2.7.8 ##

* https://mad-me.atlassian.net/browse/IOS-808

## 2.7.7 ##

* https://mad-me.atlassian.net/browse/IOS-805

## 2.7.6 ##

* https://mad-me.atlassian.net/browse/IOS-801

## 2.7.5 ##

* https://mad-me.atlassian.net/browse/IOS-801

## 2.7.4 ##

* https://mad-me.atlassian.net/browse/IOS-801

## 2.7.3 ##

* https://mad-me.atlassian.net/browse/IOS-799

* https://mad-me.atlassian.net/browse/IOS-797

## 2.7.2 ##

* https://mad-me.atlassian.net/browse/IOS-794

* https://mad-me.atlassian.net/browse/IOS-785

## 2.7.1 ##

* https://mad-me.atlassian.net/browse/IOS-779

* https://mad-me.atlassian.net/browse/IOS-778

* https://mad-me.atlassian.net/browse/IOS-777

* https://mad-me.atlassian.net/browse/IOS-776

## 2.7.0 ##

* https://mad-me.atlassian.net/browse/IOS-767

## 2.6.4 ##

* https://mad-me.atlassian.net/browse/IOS-773

## 2.6.3 ##

* https://mad-me.atlassian.net/browse/IOS-769

## 2.6.2 ##

* https://mad-me.atlassian.net/browse/IOS-763

* https://mad-me.atlassian.net/browse/IOS-760

* https://mad-me.atlassian.net/browse/IOS-759

* https://mad-me.atlassian.net/browse/IOS-758

## 2.6.1 ##

* https://mad-me.atlassian.net/browse/IOS-756

* https://mad-me.atlassian.net/browse/IOS-751

## 2.6.0 ##

* https://mad-me.atlassian.net/browse/IOS-745

* https://mad-me.atlassian.net/browse/IOS-744

* https://mad-me.atlassian.net/browse/IOS-743

* https://mad-me.atlassian.net/browse/IOS-742

* https://mad-me.atlassian.net/browse/IOS-741

* https://mad-me.atlassian.net/browse/IOS-740

* https://mad-me.atlassian.net/browse/IOS-739

* https://mad-me.atlassian.net/browse/IOS-720

## 2.5.19 ##

* https://mad-me.atlassian.net/browse/IOS-725

## 2.5.18 ##

* https://mad-me.atlassian.net/browse/IOS-723

## 2.5.17 ##

* https://mad-me.atlassian.net/browse/IOS-722

## 2.5.16 ##

* https://mad-me.atlassian.net/browse/IOS-721

## 2.5.15 ##

* https://mad-me.atlassian.net/browse/IOS-707

## 2.5.14 ##

* Minor internal changes. 

## 2.5.13 ##

* https://mad-me.atlassian.net/browse/IOS-719

## 2.5.12 ##

* https://mad-me.atlassian.net/browse/IOS-706

* https://mad-me.atlassian.net/browse/IOS-705

## 2.5.11 ##

* https://mad-me.atlassian.net/browse/IOS-711

## 2.5.10 ##

* https://mad-me.atlassian.net/browse/IOS-711

## 2.5.9 ##

* https://mad-me.atlassian.net/browse/IOS-703

## 2.5.8 ##

* https://mad-me.atlassian.net/browse/IOS-704

## 2.5.8 ##

* https://mad-me.atlassian.net/browse/IOS-704

## 2.5.7 ##

* https://mad-me.atlassian.net/browse/IOS-702

## 2.5.6 ##

* https://mad-me.atlassian.net/browse/IOS-693

* https://mad-me.atlassian.net/browse/IOS-692

* https://mad-me.atlassian.net/browse/IOS-691

* https://mad-me.atlassian.net/browse/IOS-690

* https://mad-me.atlassian.net/browse/IOS-685

* https://mad-me.atlassian.net/browse/IOS-684

* https://mad-me.atlassian.net/browse/IOS-682

* https://mad-me.atlassian.net/browse/IOS-680

* https://mad-me.atlassian.net/browse/IOS-679

* https://mad-me.atlassian.net/browse/IOS-678

* https://mad-me.atlassian.net/browse/IOS-677

* https://mad-me.atlassian.net/browse/IOS-676

* https://mad-me.atlassian.net/browse/IOS-675

* https://mad-me.atlassian.net/browse/IOS-674

* https://mad-me.atlassian.net/browse/IOS-672

* https://mad-me.atlassian.net/browse/IOS-668

* https://mad-me.atlassian.net/browse/IOS-663

* https://mad-me.atlassian.net/browse/IOS-639

* https://mad-me.atlassian.net/browse/IOS-581

## 2.5.5 ##

* https://mad-me.atlassian.net/browse/IOS-661

* https://mad-me.atlassian.net/browse/IOS-652

## 2.5.4 ##

* https://mad-me.atlassian.net/browse/IOS-658

* https://mad-me.atlassian.net/browse/IOS-657

* https://mad-me.atlassian.net/browse/IOS-656

* https://mad-me.atlassian.net/browse/IOS-655

* https://mad-me.atlassian.net/browse/IOS-654

* https://mad-me.atlassian.net/browse/IOS-653

* https://mad-me.atlassian.net/browse/IOS-652

* https://mad-me.atlassian.net/browse/IOS-651

* https://mad-me.atlassian.net/browse/IOS-649

* https://mad-me.atlassian.net/browse/IOS-648

* https://mad-me.atlassian.net/browse/IOS-646

* https://mad-me.atlassian.net/browse/IOS-645

* https://mad-me.atlassian.net/browse/IOS-644

* https://mad-me.atlassian.net/browse/IOS-643

* https://mad-me.atlassian.net/browse/IOS-642

## 2.5.3 ##

* Minor internal changes.

## 2.5.2 ##

* Minor internal changes.

## 2.5.1 ##

* Minor internal changes.

## 2.5.0 ##

* https://mad-me.atlassian.net/browse/IOS-641

* https://mad-me.atlassian.net/browse/IOS-637

* https://mad-me.atlassian.net/browse/IOS-636

* https://mad-me.atlassian.net/browse/IOS-634

* https://mad-me.atlassian.net/browse/IOS-633

* https://mad-me.atlassian.net/browse/IOS-632

* https://mad-me.atlassian.net/browse/IOS-631

* https://mad-me.atlassian.net/browse/IOS-630

* https://mad-me.atlassian.net/browse/IOS-629

* https://mad-me.atlassian.net/browse/IOS-628

* https://mad-me.atlassian.net/browse/IOS-627

* https://mad-me.atlassian.net/browse/IOS-626

* https://mad-me.atlassian.net/browse/IOS-625

* https://mad-me.atlassian.net/browse/IOS-624

* https://mad-me.atlassian.net/browse/IOS-623

* https://mad-me.atlassian.net/browse/IOS-622

* https://mad-me.atlassian.net/browse/IOS-621

* https://mad-me.atlassian.net/browse/IOS-620

* https://mad-me.atlassian.net/browse/IOS-619

* https://mad-me.atlassian.net/browse/IOS-618

* https://mad-me.atlassian.net/browse/IOS-617

* Swift 4.0.

* iOS SDK 11.1.

## 2.4.9 ##

* https://mad-me.atlassian.net/browse/IOS-613

* https://mad-me.atlassian.net/browse/IOS-612

* https://mad-me.atlassian.net/browse/IOS-611

* https://mad-me.atlassian.net/browse/IOS-610

* https://mad-me.atlassian.net/browse/IOS-609

* https://mad-me.atlassian.net/browse/IOS-608

## 2.4.8 ##

* https://mad-me.atlassian.net/browse/IOS-603

* https://mad-me.atlassian.net/browse/IOS-602

* https://mad-me.atlassian.net/browse/IOS-601

## 2.4.7 ##

* https://mad-me.atlassian.net/browse/IOS-594

* https://mad-me.atlassian.net/browse/IOS-593

* https://mad-me.atlassian.net/browse/IOS-592

* https://mad-me.atlassian.net/browse/IOS-590

* https://mad-me.atlassian.net/browse/IOS-589

* https://mad-me.atlassian.net/browse/IOS-588

* https://mad-me.atlassian.net/browse/IOS-587

* https://mad-me.atlassian.net/browse/IOS-586

* https://mad-me.atlassian.net/browse/IOS-585

* https://mad-me.atlassian.net/browse/IOS-570

## 2.4.6 ##

* https://mad-me.atlassian.net/browse/IOS-580

* https://mad-me.atlassian.net/browse/IOS-579

* https://mad-me.atlassian.net/browse/IOS-578

* https://mad-me.atlassian.net/browse/IOS-577

* https://mad-me.atlassian.net/browse/IOS-576

* https://mad-me.atlassian.net/browse/IOS-573

* https://mad-me.atlassian.net/browse/IOS-538

## 2.4.5 ##

* https://mad-me.atlassian.net/browse/IOS-572

* https://mad-me.atlassian.net/browse/IOS-571

* https://mad-me.atlassian.net/browse/IOS-567

* https://mad-me.atlassian.net/browse/IOS-566

* https://mad-me.atlassian.net/browse/IOS-565

* https://mad-me.atlassian.net/browse/IOS-564

* https://mad-me.atlassian.net/browse/IOS-562

* https://mad-me.atlassian.net/browse/IOS-561

* https://mad-me.atlassian.net/browse/IOS-560

* https://mad-me.atlassian.net/browse/IOS-558

* https://mad-me.atlassian.net/browse/IOS-536

* https://mad-me.atlassian.net/browse/IOS-534

* https://mad-me.atlassian.net/browse/IOS-521

* https://mad-me.atlassian.net/browse/IOS-520

## 2.4.4 ##

* https://mad-me.atlassian.net/browse/IOS-555

## 2.4.3 ##

* https://mad-me.atlassian.net/browse/IOS-549

* https://mad-me.atlassian.net/browse/IOS-548

* https://mad-me.atlassian.net/browse/IOS-547

* https://mad-me.atlassian.net/browse/IOS-546

* https://mad-me.atlassian.net/browse/IOS-545

* https://mad-me.atlassian.net/browse/IOS-544

* https://mad-me.atlassian.net/browse/IOS-543

* https://mad-me.atlassian.net/browse/IOS-542

* https://mad-me.atlassian.net/browse/IOS-531

* https://mad-me.atlassian.net/browse/IOS-518

* https://mad-me.atlassian.net/browse/IOS-469

## 2.4.2 ##

* https://mad-me.atlassian.net/browse/IOS-533

* https://mad-me.atlassian.net/browse/IOS-532

* https://mad-me.atlassian.net/browse/IOS-478

## 2.4.1 ##

* https://mad-me.atlassian.net/browse/IOS-529

* https://mad-me.atlassian.net/browse/IOS-528

* https://mad-me.atlassian.net/browse/IOS-527

* https://mad-me.atlassian.net/browse/IOS-525

* https://mad-me.atlassian.net/browse/IOS-524

* https://mad-me.atlassian.net/browse/IOS-523

* https://mad-me.atlassian.net/browse/IOS-519

* https://mad-me.atlassian.net/browse/IOS-513

* https://mad-me.atlassian.net/browse/IOS-512

* https://mad-me.atlassian.net/browse/IOS-511

* https://mad-me.atlassian.net/browse/IOS-510

* https://mad-me.atlassian.net/browse/IOS-509

* https://mad-me.atlassian.net/browse/IOS-508

* https://mad-me.atlassian.net/browse/IOS-507

* https://mad-me.atlassian.net/browse/IOS-506

* https://mad-me.atlassian.net/browse/IOS-505

* https://mad-me.atlassian.net/browse/IOS-502

* https://mad-me.atlassian.net/browse/IOS-501

* https://mad-me.atlassian.net/browse/IOS-500

* https://mad-me.atlassian.net/browse/IOS-499

## 2.4.0 ##

* https://mad-me.atlassian.net/browse/IOS-495

* https://mad-me.atlassian.net/browse/IOS-494

* https://mad-me.atlassian.net/browse/IOS-493

* https://mad-me.atlassian.net/browse/IOS-492

* https://mad-me.atlassian.net/browse/IOS-491

* https://mad-me.atlassian.net/browse/IOS-478

* https://mad-me.atlassian.net/browse/IOS-468

## 2.3.0 ##

* https://mad-me.atlassian.net/browse/IOS-486

* https://mad-me.atlassian.net/browse/IOS-485

* https://mad-me.atlassian.net/browse/IOS-483

* https://mad-me.atlassian.net/browse/IOS-481

* https://mad-me.atlassian.net/browse/IOS-473

* https://mad-me.atlassian.net/browse/IOS-472

* https://mad-me.atlassian.net/browse/IOS-471

* https://mad-me.atlassian.net/browse/IOS-470

* https://mad-me.atlassian.net/browse/IOS-438

## 2.2.1 ##

* https://mad-me.atlassian.net/browse/IOS-464

* https://mad-me.atlassian.net/browse/IOS-463

* https://mad-me.atlassian.net/browse/IOS-462

* https://mad-me.atlassian.net/browse/IOS-460

* https://mad-me.atlassian.net/browse/IOS-459

* https://mad-me.atlassian.net/browse/IOS-454

* https://mad-me.atlassian.net/browse/IOS-453

* https://mad-me.atlassian.net/browse/IOS-452

* https://mad-me.atlassian.net/browse/IOS-451

* https://mad-me.atlassian.net/browse/IOS-450

* https://mad-me.atlassian.net/browse/IOS-449

## 2.2.0 ##

* Swift 3.1.

* iOS SDK 10.3.

## 2.1.10 ##

* https://mad-me.atlassian.net/browse/IOS-441

* https://mad-me.atlassian.net/browse/IOS-440

* https://mad-me.atlassian.net/browse/IOS-437

* https://mad-me.atlassian.net/browse/IOS-436

* https://mad-me.atlassian.net/browse/IOS-435

* https://mad-me.atlassian.net/browse/IOS-434

## 2.1.9 ##

* https://mad-me.atlassian.net/browse/IOS-431

* https://mad-me.atlassian.net/browse/IOS-430

* https://mad-me.atlassian.net/browse/IOS-429

* https://mad-me.atlassian.net/browse/IOS-428

* https://mad-me.atlassian.net/browse/IOS-427

* https://mad-me.atlassian.net/browse/IOS-426

* https://mad-me.atlassian.net/browse/IOS-425

* https://mad-me.atlassian.net/browse/IOS-424

* https://mad-me.atlassian.net/browse/IOS-423

* https://mad-me.atlassian.net/browse/IOS-382

## 2.1.8 ##

* https://mad-me.atlassian.net/browse/IOS-420

## 2.1.7 ##

* https://mad-me.atlassian.net/browse/IOS-418

## 2.1.6 ##

* https://mad-me.atlassian.net/browse/IOS-416

## 2.1.5 ##

* https://mad-me.atlassian.net/browse/IOS-414

* https://mad-me.atlassian.net/browse/IOS-407

* https://mad-me.atlassian.net/browse/IOS-395

## 2.1.4 ##

* https://mad-me.atlassian.net/browse/IOS-412

* https://mad-me.atlassian.net/browse/IOS-408

* https://mad-me.atlassian.net/browse/IOS-401

## 2.1.3 ##

* https://mad-me.atlassian.net/browse/IOS-403

## 2.1.2 ##

* https://mad-me.atlassian.net/browse/IOS-402

* https://mad-me.atlassian.net/browse/IOS-399

* https://mad-me.atlassian.net/browse/IOS-398

* https://mad-me.atlassian.net/browse/IOS-387

## 2.1.1 ##

* https://mad-me.atlassian.net/browse/IOS-392

## 2.1.0 ##

* MMSdk API changes:  

        - func handleEventsForBackgroundURLSession(identifier: String, completionHandler: @escaping () -> Void)
        + func handleEventsForBackgroundURLSession(identifier: String, resultClosure: @escaping (_ urlSessionEventsProcessingResult: MMUrlSessionEventsProcessingResultProtocol) -> Void) 

* https://mad-me.atlassian.net/browse/IOS-385

## 2.0.15 ##

* https://mad-me.atlassian.net/browse/IOS-384

## 2.0.14 ##

* https://mad-me.atlassian.net/browse/IOS-375

## 2.0.13 ##

* https://mad-me.atlassian.net/browse/IOS-372

* https://mad-me.atlassian.net/browse/IOS-371

## 2.0.12 ##

* https://mad-me.atlassian.net/browse/IOS-369

* https://mad-me.atlassian.net/browse/IOS-368

## 2.0.11 ##

* https://mad-me.atlassian.net/browse/IOS-363

* https://mad-me.atlassian.net/browse/IOS-362

* https://mad-me.atlassian.net/browse/IOS-361

## 2.0.10 ##

* https://mad-me.atlassian.net/browse/IOS-358

* https://mad-me.atlassian.net/browse/IOS-357

* https://mad-me.atlassian.net/browse/IOS-352

* https://mad-me.atlassian.net/browse/IOS-351

* https://mad-me.atlassian.net/browse/IOS-346

* https://mad-me.atlassian.net/browse/IOS-339

## 2.0.9 ##

* https://mad-me.atlassian.net/browse/IOS-346

* https://mad-me.atlassian.net/browse/IOS-345

* https://mad-me.atlassian.net/browse/IOS-344

* https://mad-me.atlassian.net/browse/IOS-343

* https://mad-me.atlassian.net/browse/IOS-340

* https://mad-me.atlassian.net/browse/IOS-319

## 2.0.8 ##

* Minor internal (dependency) changes. 

## 2.0.7 ##

* https://mad-me.atlassian.net/browse/IOS-332

* https://mad-me.atlassian.net/browse/IOS-331

* https://mad-me.atlassian.net/browse/IOS-330

* https://mad-me.atlassian.net/browse/IOS-329

* https://mad-me.atlassian.net/browse/IOS-328

* https://mad-me.atlassian.net/browse/IOS-327

* https://mad-me.atlassian.net/browse/IOS-321

* https://mad-me.atlassian.net/browse/IOS-318

* https://mad-me.atlassian.net/browse/IOS-317

* https://mad-me.atlassian.net/browse/IOS-312

* https://mad-me.atlassian.net/browse/IOS-303

## 2.0.6 ##

* https://mad-me.atlassian.net/browse/IOS-325

* https://mad-me.atlassian.net/browse/IOS-324

* https://mad-me.atlassian.net/browse/IOS-323

* https://mad-me.atlassian.net/browse/IOS-322

* https://mad-me.atlassian.net/browse/IOS-320

* https://mad-me.atlassian.net/browse/IOS-319

* https://mad-me.atlassian.net/browse/IOS-316

* https://mad-me.atlassian.net/browse/IOS-315

* https://mad-me.atlassian.net/browse/IOS-314

* https://mad-me.atlassian.net/browse/IOS-313

* https://mad-me.atlassian.net/browse/IOS-311

* https://mad-me.atlassian.net/browse/IOS-310

* https://mad-me.atlassian.net/browse/IOS-309

* https://mad-me.atlassian.net/browse/IOS-307

* https://mad-me.atlassian.net/browse/IOS-306

* https://mad-me.atlassian.net/browse/IOS-305

* https://mad-me.atlassian.net/browse/IOS-304

* https://mad-me.atlassian.net/browse/IOS-302

* https://mad-me.atlassian.net/browse/IOS-300

* https://mad-me.atlassian.net/browse/IOS-299

## 2.0.5 ##

* Minor internal changes. 

## 2.0.4 ##

* https://mad-me.atlassian.net/browse/IOS-295

* https://mad-me.atlassian.net/browse/IOS-294

* https://mad-me.atlassian.net/browse/IOS-291

## 2.0.3 ##

* https://mad-me.atlassian.net/browse/IOS-288

* https://mad-me.atlassian.net/browse/IOS-286

* https://mad-me.atlassian.net/browse/IOS-282

* https://mad-me.atlassian.net/browse/IOS-281

* https://mad-me.atlassian.net/browse/IOS-280

* https://mad-me.atlassian.net/browse/IOS-279

* https://mad-me.atlassian.net/browse/IOS-278

* https://mad-me.atlassian.net/browse/IOS-277

* https://mad-me.atlassian.net/browse/IOS-258

## 2.0.2 ##

* Swift 3.0.1. 

* https://mad-me.atlassian.net/browse/IOS-274

* https://mad-me.atlassian.net/browse/IOS-273

* https://mad-me.atlassian.net/browse/IOS-272

* https://mad-me.atlassian.net/browse/IOS-270

* https://mad-me.atlassian.net/browse/IOS-269

* https://mad-me.atlassian.net/browse/IOS-268

## 2.0.1 ##

* Minor internal changes. 

## 2.0.0 ##

* https://mad-me.atlassian.net/browse/IOS-263

* https://mad-me.atlassian.net/browse/IOS-260

* https://mad-me.atlassian.net/browse/IOS-259

* https://mad-me.atlassian.net/browse/IOS-256

* https://mad-me.atlassian.net/browse/IOS-255

* https://mad-me.atlassian.net/browse/IOS-252

* https://mad-me.atlassian.net/browse/IOS-250

* https://mad-me.atlassian.net/browse/IOS-249

* https://mad-me.atlassian.net/browse/IOS-248

* https://mad-me.atlassian.net/browse/IOS-247

* https://mad-me.atlassian.net/browse/IOS-246

* https://mad-me.atlassian.net/browse/IOS-245

* https://mad-me.atlassian.net/browse/IOS-243

* https://mad-me.atlassian.net/browse/IOS-242

* https://mad-me.atlassian.net/browse/IOS-241

* https://mad-me.atlassian.net/browse/IOS-240

* https://mad-me.atlassian.net/browse/IOS-239

* https://mad-me.atlassian.net/browse/IOS-238

* https://mad-me.atlassian.net/browse/IOS-237

* https://mad-me.atlassian.net/browse/IOS-236

* https://mad-me.atlassian.net/browse/IOS-172

## 1.2.2 ##

* Swift 3.0. 

## 1.2.1 ##

* Updated framework podspec. 

## 1.2.0 ##

* https://mad-me.atlassian.net/browse/IOS-243

* https://mad-me.atlassian.net/browse/IOS-242

* https://mad-me.atlassian.net/browse/IOS-241

* https://mad-me.atlassian.net/browse/IOS-240

* https://mad-me.atlassian.net/browse/IOS-239

* https://mad-me.atlassian.net/browse/IOS-238

* https://mad-me.atlassian.net/browse/IOS-237

* https://mad-me.atlassian.net/browse/IOS-236

## 1.1.35 ##

* https://mad-me.atlassian.net/browse/IOS-232

* https://mad-me.atlassian.net/browse/IOS-231

* https://mad-me.atlassian.net/browse/IOS-230

* https://mad-me.atlassian.net/browse/IOS-229

* https://mad-me.atlassian.net/browse/IOS-228

* https://mad-me.atlassian.net/browse/IOS-227

## 1.1.34 ##

* https://mad-me.atlassian.net/browse/IOS-224

* https://mad-me.atlassian.net/browse/IOS-221

* https://mad-me.atlassian.net/browse/IOS-220

* https://mad-me.atlassian.net/browse/IOS-111

## 1.1.33 ##

* https://mad-me.atlassian.net/browse/IOS-218

## 1.1.32 ##

* New survey UI. 

* Updated Readme.md. 

* Other improvements. 

## 1.1.30 ##

* Use the iphoneos Info.plist.  

## 1.1.29 ##

* Enable bitcode. 

## 1.1.28 ##

* New MMSdk API to signal that application protected data became available. 

## 1.1.27 ##

## 1.1.26 ##

### Notes ###

* Support direct and notification-based ad presentations. 

* New MMSdk APIs for notification and url processing, for handling background url session events and for displaying various view controllers. 

### Removed ###

* MMDefaultAdSelectionContext. 

* MMTagAdSelectionFilter. 

* MMAdSelectionContextProtocol. 

* MMAdSelectionFilterProtocol. 

### Added ###

* MMHomeScreenOption. 

* MMNotificationProcessingError. 

* MMUrlProcessingError. 

* MMAdPresentationMode. 

* MMHomeScreenOptionType. 

* MMNotificationProcessingResultState. 

* MMUrlProcessingResultState. 

* MMNotificationProcessingResultProtocol. 

* MMUrlProcessingResultProtocol. 

* MMIdentifier. 

### Updated ###

* MMAdPresentationError. 

* MMSdk. 

* MMAdPresentationResultState. 

* MMAdPresentationResultProtocol. 

