Pod::Spec.new do |s|
s.name         = "madme_ios_sdk_framework"
s.version      = "6.2.1"
s.summary      = "This is the mAdme iOS framework."
s.homepage     = "http://www.mad-me.com"
s.license      = { :type => "Copyright", :text => "Copyright 2022 mAdme." }
s.author       = "mAdme"
s.platform     = :ios, "11.0"
s.requires_arc = true
s.swift_version = "5.6"

s.source       = { :git => "https://bitbucket.org/madme/madme_ios_sdk_framework5.git", :tag => "#{s.version}" }

s.dependency 'Fuzi', '3.1.3'
s.dependency 'ReachabilitySwift', '5.0.0'

# Fuzi
s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }

# Define a default_subspec.
s.default_subspec = 'Debug+RSA'

#
# RSA-based
#

s.subspec "Debug+RSA" do |sd_rsa|
sd_rsa.vendored_frameworks = 'Debug+RSA/madme_ios_sdk.xcframework'
sd_rsa.dependency 'SwiftyRSA', '1.7.0'
sd_rsa.dependency 'madme_ios_sdk_shared_framework/Debug', '4.3.4'
end

s.subspec "Debug+RSA-CallKit" do |sd_rsack|
sd_rsack.vendored_frameworks = 'Debug+RSA-CallKit/madme_ios_sdk.xcframework'
sd_rsack.dependency 'SwiftyRSA', '1.7.0'
sd_rsack.dependency 'madme_ios_sdk_shared_framework/Debug', '4.3.4'
end

s.subspec "Debug+RSA+OptionalUI" do |sd_rsaou|
sd_rsaou.vendored_frameworks = 'Debug+RSA+OptionalUI/madme_ios_sdk.xcframework'
sd_rsaou.dependency 'SwiftyRSA', '1.7.0'
sd_rsaou.dependency 'madme_ios_sdk_shared_framework/Debug', '4.3.4'
end

s.subspec "Release+RSA" do |sr_rsa|
sr_rsa.vendored_frameworks = 'Release+RSA/madme_ios_sdk.xcframework'
sr_rsa.dependency 'SwiftyRSA', '1.7.0'
sr_rsa.dependency 'madme_ios_sdk_shared_framework/Release', '4.3.4'
end

s.subspec "Release+RSA-CallKit" do |sr_rsack|
sr_rsack.vendored_frameworks = 'Release+RSA/madme_ios_sdk.xcframework'
sr_rsack.dependency 'SwiftyRSA', '1.7.0'
sr_rsack.dependency 'madme_ios_sdk_shared_framework/Release', '4.3.4'
end

s.subspec "Release+RSA+OptionalUI" do |sr_rsaou|
sr_rsaou.vendored_frameworks = 'Release+RSA+OptionalUI/madme_ios_sdk.xcframework'
sr_rsaou.dependency 'SwiftyRSA', '1.7.0'
sr_rsaou.dependency 'madme_ios_sdk_shared_framework/Release', '4.3.4'
end

#
# AES-based
#

s.subspec "Debug+AES" do |sd_aes|
sd_aes.vendored_frameworks = 'Debug+AES/madme_ios_sdk.xcframework'
sd_aes.dependency 'madme_ios_sdk_shared_framework/Debug', '4.3.4'
end

s.subspec "Debug+AES+OptionalUI" do |sd_aesou|
sd_aesou.vendored_frameworks = 'Debug+AES+OptionalUI/madme_ios_sdk.xcframework'
sd_aesou.dependency 'madme_ios_sdk_shared_framework/Debug', '4.3.4'
end

s.subspec "Release+AES" do |sr_aes|
sr_aes.vendored_frameworks = 'Release+AES/madme_ios_sdk.xcframework'
sr_aes.dependency 'madme_ios_sdk_shared_framework/Release', '4.3.4'
end

s.subspec "Release+AES+OptionalUI" do |sr_aesou|
sr_aesou.vendored_frameworks = 'Release+AES+OptionalUI/madme_ios_sdk.xcframework'
sr_aesou.dependency 'madme_ios_sdk_shared_framework/Release', '4.3.4'
end

end
